'use strict';

angular.module('fizzbuzz_ui')
  .controller('UserCtrl', [
    'UserFactory',
    '$scope',
    '$location',
    '$rootScope',
    function(UserFactory, $scope, $location, $rootScope) {
      $scope.confirm_password = null;
      $scope.user             = { };
      $scope.error_msg        = null;

      $scope.userSignUp = function(user) {
        UserFactory.register.save(user).$promise.then(
          function successCallback(response) {
            localStorage.user = JSON.stringify(response);
            $rootScope.isAuthenticated = function(){ return true };
            
            $location.path('/');
          },
          
          function errorCallback(response) {
            $scope.error_msg = response.data.error
          }
        )
      }

      $scope.userLogin = function(user) {
        UserFactory.session.save(user).$promise.then(
          function successCallback(response) {
            localStorage.user = JSON.stringify(response);
            $rootScope.isAuthenticated = function(){ return true };

            $location.path('/');
          },

          function errorCallback(response) {
            $scope.error_msg = response.data.error
          }
        )
      }
    }
  ]);
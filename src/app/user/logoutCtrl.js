'use strict';

angular.module('fizzbuzz_ui')
  .controller('LogoutController',['$location',
    function($location){
      localStorage.clear();
      $location.path('/login');
    }
  ]);
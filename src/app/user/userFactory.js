'use strict';

angular.module('fizzbuzz_ui')
  .factory('UserFactory', [
    '$resource',
    'ApiService',
    function ($resource, ApiService) {
      return {
        register: $resource(ApiService.getEndpoint() + 'api/v1/registrations'),
        session: $resource(ApiService.getEndpoint() + 'api/v1/sessions')
      }
    }
  ]);
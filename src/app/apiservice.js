'use strict';

angular.module('fizzbuzz_ui')
  .factory('ApiService',
  [
    function() {
      return {
        getEndpoint: function() {
          return 'http://localhost:3000/';
        }
      };
    }
  ]);

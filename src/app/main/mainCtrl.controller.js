/**
 * @ngdoc controller
 * @name fizzbuzz_ui.controller:mainCtrl
 *
 * @description
 * Controller mainCtrl
 *
 * **Note:** documentation needs to be updated as logic is added
 *
 */
angular.module('fizzbuzz_ui')
  .controller('mainCtrl', [
    'FizzBuzzFactory',
    '$scope',
    'spinnerService',
    '$http',
    '$location',
    function (
      FizzBuzzFactory,
      $scope,
      spinnerService,
      $http,
      $location) {
      var user = localStorage.user;

      if(user !== undefined) {
        user = JSON.parse(user);
        $http.defaults.headers.common['X-Access-Token'] = user.auth_token
      }
      else {
        $location.path('/login')
      }

      $scope.results  = [];
      $scope.pageSize = 47;
      
      FizzBuzzFactory.query().$promise.then(
        function successCallback(response) {
          $scope.results = response;
        }
      );


      
      $scope.buttonCss = function(result) {
        if(result.favourite === true) {
          return "btn btn-muted btn-sm btn-block"
        }
        else {
          return "btn btn-success btn-sm btn-block"
        }
      };
      
      $scope.buttonText = function(result) {
        if(result.favourite === true) {
          return "Favourite"
        }
        else {
          return "Change To Favourite"
        }
      };

      $scope.makeFavourite = function(result) {
        FizzBuzzFactory.save({ favourite: result.value }).$promise.then(
          function successCallback(_response) {
            result.favourite = true;
          },
          function errorCallback(response) {
            // Something went wrong message
          }
        )
      };

      $scope.previousPage = function (pageSize) {
        spinnerService.show('fizzbuzzSpinner');
        var results = $scope.results;
        var min     = (results[0].value - pageSize);

        min = min < 1 ? 1 : min;

        var max     = results[0].value;

        FizzBuzzFactory.query(
          {
            min: min,
            max: max,
            page_size: pageSize
          }
        ).$promise.then(
          function successCallback(response) {
            $scope.results = response;

            spinnerService.hide('fizzbuzzSpinner');
          }
        );
      };

      $scope.nextPage = function (pageSize) {
        spinnerService.show('fizzbuzzSpinner');

        var results = $scope.results;
        var min     = results[results.length - 1].value;
        var max     = (min * pageSize);

        FizzBuzzFactory.query(
          {
            min: min,
            max: max,
            page_size: pageSize
          }
        ).$promise.then(
          function successCallback(response) {
            $scope.results = response;

            spinnerService.hide('fizzbuzzSpinner');
          }
        );
      };
    }]);

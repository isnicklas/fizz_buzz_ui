'use strict';

angular.module('fizzbuzz_ui')
  .factory('FizzBuzzFactory', [
    '$resource',
    'ApiService',
    function ($resource, ApiService) {
      return $resource(ApiService.getEndpoint())
    }
  ]);
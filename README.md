### Fizz Buzz UI

#### User Flow

1. Create an account / login

2. Select your favourite numbers

3. You can reduce or increase the page size

#### Starting Application

1. Start the fizz buzz api on `port 3000`

2. Run `gulp serve`

Enjoy testing my test.


